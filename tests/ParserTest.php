<?php

namespace Tests\DerbyCsv;

use DerbyCsv\Parser;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase {
    /**
     * @param string $csv
     * @param array $expected
     * @dataProvider provideParseInput
     */
    public function testParse(string $csv, array $expected)
    {
        $fp = fopen('php://memory','r+');
        fwrite($fp, $csv);
        rewind($fp);

        $parser = Parser::fromStream($fp);
        $rows = iterator_to_array($parser->parse());
        $this->assertEquals($expected, $rows);
    }

    public function testHeader()
    {
        $fp = fopen('php://memory','r+');
        fwrite($fp, "0,2,3");
        rewind($fp);

        $parser = Parser::fromStream($fp)->setHeader("c1", "c2", "c3");
        $rows = iterator_to_array($parser->parse());
        $expected = [
            [
                "c1" => 0,
                "c2" => 2,
                "c3" => 3,
            ]
        ];
        $this->assertEquals($expected, $rows);
    }

    public function provideParseInput()
    {
        return [
            [
                "0,1",
                [
                    [0, 1]
                ],
            ],
            [
                "0,1\n",
                [
                    [0, 1]
                ],
            ],
            [
                "0,1.1,0.2,5",
                [
                    [0,1.1,0.2,5]
                ],
            ],
            [
                ",0,\"\"",
                [
                    [null, 0, ""]
                ],
            ],
            [
                '"str"',
                [
                    ["str"]
                ],
            ],
            [
                '"string with escaped "" quotes"',
                [
                    ["string with escaped \" quotes"]
                ],
            ],
            [
                "\"multi\nline\"",
                [
                    ["multi\nline"]
                ],
            ],
            [
                '"line 1",25,"kg"' . "\n" . '"line 2",43,"g"',
                [
                    ["line 1", 25, "kg"],
                    ["line 2", 43, "g"],
                ],
            ],
            [
                '59108,59081,26,41380,477,1,41381,7.63546798029557E-5,,0,304084,"Reported as 0.003 MJ /tonne NSP.  Converted to kg using the lower heating value (40.6 MJ/kg) reported in ""Dones R. et al.(2007) Life Cycle Inventories of Energy Systems: Results for Current Systems in Switzerland and other UCTE Countries. Final report ecoinvent data v2.0, No. 5. Swiss Centre for Life Cycle Inventories, D?bendorf, CH.""",1.0536945812807889E-5,,547,1,7.63546798029557E-5,,1.0247973619913604,,,,"("',
                [
                    [59108, 59081, 26,41380,477,1,41381,7.63546798029557E-5,null,0,304084,'Reported as 0.003 MJ /tonne NSP.  Converted to kg using the lower heating value (40.6 MJ/kg) reported in "Dones R. et al.(2007) Life Cycle Inventories of Energy Systems: Results for Current Systems in Switzerland and other UCTE Countries. Final report ecoinvent data v2.0, No. 5. Swiss Centre for Life Cycle Inventories, D?bendorf, CH."',1.0536945812807889E-5,null,547,1,7.63546798029557E-5,null,1.0247973619913604,null,null,null,"("]
                ],
            ],
        ];
    }
}